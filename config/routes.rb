Rails.application.routes.draw do
  get 'manager/index'
  devise_for :users
  resources :users
  resources :lines
  resources :orders do
    resources :payments, only: [:new] do
      collection do
        get 'success'
      end
    end

    member do
      post 'produce'
      post 'deliver'
    end
  end


  resources :tables do
    member do
      get 'qrcode'
    end
  end
  resources :roles
  resources :venues do
    member do
      get 'produce'
      get 'deliver'
    end
  end
  resources :items do
    member do
      get 'select'
    end
    collection do
      get 'menu'
    end
  end

  get 'admin/index'
  get 'homepage/index'
  root 'homepage#index'
end
