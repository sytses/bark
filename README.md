Bark is an app for ordering in the food and beverage industry.

For the why of this project see the [blog post](https://sytse.com/2020/03/21/wordpress-for-food-and-beverage.html).

If you are interested please fill out our [contact form](https://docs.google.com/forms/d/e/1FAIpQLScw2Lujb6ZypgCy_7uDjaWfX_1KtHxBvccQLOodxKPG8aeFMA/viewform).

## Application Concepts

1. `User`: An individual user of the application.
1. `Role`: links a `User` to a `Venue` as one of the following: as a manager who can change items, back of house who can fulfill orders, and/or front of house who can deliver orders.
1. `Venue`: Your individual place(s) of business where you sell food.
1. `Table`: A specific table within one of your venues; associated with a scannable QR code that your customers can use to not have to know their table ID.
1. `Item`: A choice on the menu at one of your venues.
1. `Order`: A specific order made by a table - can be ordered, made, delivered, and paid.
1. `Line`: A line item on an `Order` with its price and link to the the food `Item` that was chosen, along with any special instructions.

Each of the above concepts is represented by a table in the database schema.

All records can be archived, but not deleted, in order avoid breaking the database relations.
For the detailed database schema see [schema.rb](https://gitlab.com/sytses/bark/-/blob/master/db/schema.rb).

## Three flows

1. As a customer I want to place an order (select and pay).
1. As a back of house staff I want to process the order (see and make).
1. As a front of house I want to deliver the order (see and deliver).

## Setting up a new restaurant

If you're setting up a restaurant for the first time, there's a few things you'll want to do in order. Be sure to familiarize yourself with the concepts and flows above first.

_Note that, in the current version, when you are asked to select a venue, item, etc. you must provide its numeric ID. This will be updated in the future to be more easily selectable by name._

1. Register an account to sign in
1. From the manager interface, create your first venue (i.e., your main restaurant).
1. Add yourself to the appropriate roles (manager, back/front of house)
1. Add the tables in your restaurant (Venue). These are individual places that food might be sent to.
1. Add food items in your restaurant (Venue) that your customers can order.

At this point you have everything set up. You can create a test order and then add line items to it through the manager interface.

_The customer, back of house, and front of house interfaces are not yet implemented._

## Keep it simple

We're keeping things as simple as possible:

1. Hosting on Heroku
1. PostgreSQL as the only datastore (and Redis at some point)
1. Rails ActiveStorage for attachments hosted on GCP
1. Rails web views (maybe React Native at some point)
1. Authentification with Devise (longer timeouts than standard for usability)
1. Simple text for now (rich text later with Action Text)
1. Admin interface with Rails scaffold
1. Payments via Stripe

## Ghost orders

To make this quick to use for customers they don't have to download an app, create an account, or even enter an email.
That means that we'll have users without users, since users need an email.
When a payment is made we'll likely get the email address from the payment provider and can link the user at that time, although I'm not sure if Apple Pay sends it.

We could store the order completely in the cookie but that serialization of line instructions (no onions, etc.) makes upgrades more difficult and would require us to recheck the data after payment because you can't trust client side data. So we'll store the order id in the cookie and leave it at that. Of course we also store the table in the cookie. We can't use the table to lookup an order because multiple people might be ordering from the same table and there might be abandoned orders from previous customers.

People can log in before placing an order to reorder an old order. And after ordering people can enter an email to create an account, if the payment provider didn't send us an email.

Ghost orders that didn't get paid (and not made nor delivered) will be helpful to diagnose and retarget similar to abandoned shopping carts at e-commerce sites.

## Payment

If you run this software yourself you link your own payment provider (Stipe, Adyen, etc.).
If you run the SaaS version at barkorder.com the payment is processed for you and you get a card debit for the amount after processing fees and service fees to run the site.

## Helpful commands

Be sure to [set up your development environment](#setting-up-your-development-environment) first.

1. rails server
1. rails db:migrate:reset && rails db:fixtures:load

## Project status

1. Need to add restrictions to who can access pages.
1. Need to add restrictions so user can only see and modify their own data.
1. No styling is applied yet.

## Setting up your development environment

1. Fork this project to your own namespace
1. Clone a local copy
1. Install RVM: https://rvm.io/rvm/install
1. Install Yarn: https://yarnpkg.com/getting-started/install
1. Install Node: https://nodejs.org/en/download/package-manager/
1. Install appropriate Ruby version (see .ruby-version then run `rvm install x.x.x`)
1. Switch to correct Ruby version (`rvm use x.x.x`)
1. Run `bundle install` from your local clone
1. Run `yarn install --check-files`
