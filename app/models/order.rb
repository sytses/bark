class Order < ApplicationRecord
  belongs_to :user, required: false # See the Readme section about Ghost orders
  belongs_to :table
  has_one :venue, through: :table
  has_many :lines, dependent: :destroy
  has_many :items

  def add(item)
    lines.create!(item: item, price: item.price)
  end

  def total
    lines.sum(&:price)
  end

  def amount
    (total * 100).to_i
  end

  def pay
    self.paid = DateTime.current
    save!
  end

  def produce
    self.produced = DateTime.current
    save!
  end

  def deliver
    self.delivered = DateTime.current
    save!
  end
end
