class Table < ApplicationRecord
  belongs_to :venue
  has_many :orders
end
