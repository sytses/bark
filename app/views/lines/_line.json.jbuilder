json.extract! line, :id, :order_id, :item_id, :price, :instructions, :created_at, :updated_at
json.url line_url(line, format: :json)
