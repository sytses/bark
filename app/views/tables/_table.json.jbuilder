json.extract! table, :id, :description, :venue_id, :created_at, :updated_at
json.url table_url(table, format: :json)
