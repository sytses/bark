json.extract! role, :id, :user_id, :venue_id, :manager, :back, :front, :created_at, :updated_at
json.url role_url(role, format: :json)
