json.extract! order, :id, :user_id, :table_id, :paid, :made, :delivered, :created_at, :updated_at
json.url order_url(order, format: :json)
