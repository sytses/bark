class PaymentsController < ApplicationController
  before_action :set_order

  def new
    if @order.paid
      flash[:notice] = "This order has already been paid"
      redirect_to order_path(@order)
    end
    @load_stripe = true
    @session = Stripe::Checkout::Session.create(
      payment_method_types: ['card'],
      line_items: [{
        name: "Order #{@order.id}",
        description: "Payment for order at #{@order.venue.name} for order #{@order.id}",
        amount: @order.amount,
        currency: 'EUR',  # TODO: We have a hardcoded currency
        quantity: 1,
      }],
      success_url: success_order_payments_url(@order),
      cancel_url: order_url(@order) # TODO: we probably want a different URL when it is canceled
    )
  end

  def success
    # TODO: can we create a customer here - we have the email and name from Stripe now
    @order.pay
    flash[:success] = "The order has been paid and will be delivered soon."
    redirect_to order_path(@order)
  end

  private

  def set_order
    @order = Order.find(params[:order_id])
  end
end