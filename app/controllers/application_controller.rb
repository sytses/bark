class ApplicationController < ActionController::Base
    def is_admin?
        # check if user is a admin
        # if not admin then redirect to where ever you want
        redirect_to(root_path, alert: "Redirecting since you are not an admin.") unless current_user.admin?
    end
end
