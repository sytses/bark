class OrderWithoutUser < ActiveRecord::Migration[6.0]
  def change
    remove_foreign_key :orders, :users
  end
end
