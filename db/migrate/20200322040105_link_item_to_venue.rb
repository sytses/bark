class LinkItemToVenue < ActiveRecord::Migration[6.0]
  def change
    add_column :items, :venue_id, :integer
    add_foreign_key :items, :venues
  end
end
