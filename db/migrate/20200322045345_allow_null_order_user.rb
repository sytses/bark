class AllowNullOrderUser < ActiveRecord::Migration[6.0]
  def change
    change_column_null(:orders, 'user_id', true) # :user gives an error: undefined method `null=' for nil:NilClass
  end
end
