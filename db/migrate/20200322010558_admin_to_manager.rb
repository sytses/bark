class AdminToManager < ActiveRecord::Migration[6.0]
  def change
    rename_column :roles, :admin, :manager
  end
end
