class CreateLines < ActiveRecord::Migration[6.0]
  def change
    create_table :lines do |t|
      t.belongs_to :order, null: false, foreign_key: true
      t.belongs_to :item, null: false, foreign_key: true
      t.decimal :price
      t.text :instructions

      t.timestamps
    end
  end
end
