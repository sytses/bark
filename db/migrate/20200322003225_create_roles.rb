class CreateRoles < ActiveRecord::Migration[6.0]
  def change
    create_table :roles do |t|
      t.belongs_to :user, null: false, foreign_key: true
      t.belongs_to :venue, null: false, foreign_key: true
      t.boolean :admin # Will be renamed to manager later.
      t.boolean :back
      t.boolean :front

      t.timestamps
    end
  end
end
