class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.belongs_to :user, null: false, foreign_key: true
      t.belongs_to :table, null: false, foreign_key: true
      t.datetime :paid
      t.datetime :produced
      t.datetime :delivered

      t.timestamps
    end
  end
end
